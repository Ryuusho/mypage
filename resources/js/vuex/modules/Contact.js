export default {
    state: {
        data: [],
    },
    actions: {
        saveContact(context,payload) {
            return new Promise((resolve,reject)=> {
                axios.post('/api/contact',payload)
                    .then(response => {
                        context.commit('saveContact', response.data)
                        resolve(response)
                    })
                    .catch(error => {
                        console.log(error)
                        reject(error)
                    })
            });
         
        }
    },
    getters: {
        //       
    },
    mutations: {
        saveContact(state, payload) {
            Vue.set(state, 'data', payload)
        }
    },
}
