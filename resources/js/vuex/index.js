import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


import Contact from './modules/Contact'


export default new Vuex.Store({
	modules: {
		Contact
	},
})

