@extends('layouts.app')
    @section('title') JoseSB - Personal Portfolio Template @endsection

@section('content')
    @include('layouts.navbar')
    @include('layouts.headerImage')
    @include('layouts.about')
    @include('layouts.counter')
    @include('layouts.service')
    @include('layouts.ourSkill')
    @include('layouts.portafolio')
    
    {{-- @include('layouts.testimonial') --}}
    @include('layouts.blog')

    <contact-us/>
@endsection