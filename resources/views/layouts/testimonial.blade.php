    <!-- Testimonial -->
       <section class="section testimonial-section">
          <div class="container">
            <div class="row justify-content-center m-50px-b md-m-40px-b sm-m-25px-b">
              <div class="col-12 col-md-10 col-lg-7">
                <div class="section-title text-center">
                  <h2 class="font-alt">What People Say?</h2>
                  <p>I design and develop services for customers of all sizes, specializing in creating stylish, modern websites, web services and online stores. My passion is to design digital user experiences through the bold interface.</p>
                </div>
              </div>
            </div>


            <div class="row justify-content-center">
              <div class="col-md-12">
                  <div id="client-slider-single" class="owl-carousel owl-loaded owl-drag">
                     <!-- col -->

                     <!-- col -->

                     <!-- col -->

                     <!-- col -->

                  <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-1110px, 0px, 0px); transition: all 0s ease 0s; width: 3700px;"><div class="owl-item cloned" style="width: 370px;"><div class="testimonial-col">
                        <div class="img">
                          <img src="/image/template/avtar2.jpg" alt="Ryan" title="Ryan">
                        </div>
                        <h6>Jennifer Lutheran</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div></div><div class="owl-item cloned" style="width: 370px;"><div class="testimonial-col">
                        <div class="img">
                          <img src="/image/template/avtar3.jpg" alt="Ryan" title="Ryan">
                        </div>
                        <h6>Jennifer Lutheran</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div></div><div class="owl-item cloned" style="width: 370px;"><div class="testimonial-col">
                        <div class="img">
                          <img src="/image/template/avtar1.jpg" alt="Ryan" title="Ryan">
                        </div>
                        <h6>Jennifer Lutheran</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div></div><div class="owl-item active" style="width: 370px;"><div class="testimonial-col">
                        <div class="img">
                          <img src="/image/template/avtar1.jpg" alt="Ryan" title="Ryan">
                        </div>
                        <h6>Jennifer Lutheran</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div></div><div class="owl-item active" style="width: 370px;"><div class="testimonial-col">
                        <div class="img">
                          <img src="/image/template/avtar2.jpg" alt="Ryan" title="Ryan">
                        </div>
                        <h6>Jennifer Lutheran</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div></div><div class="owl-item active" style="width: 370px;"><div class="testimonial-col">
                        <div class="img">
                          <img src="/image/template/avtar3.jpg" alt="Ryan" title="Ryan">
                        </div>
                        <h6>Jennifer Lutheran</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div></div><div class="owl-item" style="width: 370px;"><div class="testimonial-col">
                        <div class="img">
                          <img src="/image/template/avtar1.jpg" alt="Ryan" title="Ryan">
                        </div>
                        <h6>Jennifer Lutheran</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div></div><div class="owl-item cloned" style="width: 370px;"><div class="testimonial-col">
                        <div class="img">
                          <img src="/image/template/avtar1.jpg" alt="Ryan" title="Ryan">
                        </div>
                        <h6>Jennifer Lutheran</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div></div><div class="owl-item cloned" style="width: 370px;"><div class="testimonial-col">
                        <div class="img">
                          <img src="/image/template/avtar2.jpg" alt="Ryan" title="Ryan">
                        </div>
                        <h6>Jennifer Lutheran</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div></div><div class="owl-item cloned" style="width: 370px;"><div class="testimonial-col">
                        <div class="img">
                          <img src="/image/template/avtar3.jpg" alt="Ryan" title="Ryan">
                        </div>
                        <h6>Jennifer Lutheran</h6>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div></div></div></div><div class="owl-nav disabled"><div class="owl-prev">prev</div><div class="owl-next">next</div></div><div class="owl-dots"><div class="owl-dot active"><span></span></div><div class="owl-dot"><span></span></div></div></div> <!-- owl -->
              </div> <!-- col -->
            </div> <!-- row -->
          </div> <!-- container -->
        </section>
        <!--  Testimonial End  -->