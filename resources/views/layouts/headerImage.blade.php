<!-- Home Banner Start -->
<section id="home" class="home-banner gray-bg" style="background-image: url(/../image/template/overlay-2.jpg);">
    <div class="sec-round">
        <div class="container full-height">
            <div class="round-item round-item-1"></div>
            <div class="round-item round-item-2"></div>
            <div class="round-item round-item-3"></div>
            <div class="round-item round-item-4"></div>
            <div class="round-item round-item-5"></div>
        </div>
    </div>

    <div class="container">
        <div class="row full-screen align-items-center">
            <div class="col col-md-12 col-lg-8 col-xl-6 p-80px-tb">
                <div class="home-text-center theme-after m-50px-t">
                    <h4>Hello, my name is</h4>
                    <h5 class="font-alt">José Sabina</h5>
                    <p>I'm web application developer graduated in Venezuela. I make the visual to be more interactive.</p>
                    
                    <div class="btn-bar">
                        <a href="/download" class="m-btn-link m-btn-link-white theme-after">Download CV</a>
                    </div> 
                </div> <!-- home-text-center -->
            </div> <!-- col -->
        </div>

    </div><!-- container -->
    <a href="#aboutus" data-scroll="smooth" class="mouse-icon hidden-sm"><span class="wheel"></span></a>
    <vue-particles 
        color="#dedede"
        :particle-opacity="0.7"
        :particles-number="80"
        shape-type="circle"
        :particle-size="4"
        lines-color="#dedede"
        :lines-width="1"
        :line-linked="true"
        :line-opacity="0.4"
        :lines-distance="150"
        :move-speed="3"
        :hover-effect="true"
        hover-mode="grab"
        :click-effect="true"
        click-mode="push"></vue-particles>

</section>
<!-- Home Banner End -->