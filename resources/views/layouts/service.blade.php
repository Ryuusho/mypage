<!-- Service Start -->
<section id="services" class="section gray-bg services-section">
    <div class="container">
        <div class="row justify-content-center m-45px-b md-m-30px-b sm-m-25px-b">
            <div class="col-12 col-md-10 col-lg-7">
                <div class="section-title text-center">
                    <h2 class="font-alt">My Services</h2>
                    <p>I design and develop services for customers of all sizes, specializing in creating stylish, modern websites, web services and online stores. My passion is to design digital user experiences through the bold interface.</p>
                </div>
            </div>
        </div> <!-- row -->

        <div class="row">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="feature-box-02">
                    <i class="icon theme-bg ti-ruler-pencil"></i>
                    <div class="feature-content">
                        <h5>Development</h5>
                        <p>Jobs, queues, events configuration to give more power to web applications.</p>
                        <div class="read-more">
                            <a href="#" class="more-btn">Read More <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div> <!-- feature-box-02 -->
            </div> <!-- col -->

            <div class="col-12 col-md-6 col-lg-4">
                <div class="feature-box-02">
                    <i class="icon theme-bg ti-image"></i>
                    <div class="feature-content">
                        <h5>Graphic</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <div class="read-more">
                            <a href="#" class="more-btn">Read More <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div> <!-- feature-box-02 -->
            </div> <!-- col -->

            <div class="col-12 col-md-6 col-lg-4">
                <div class="feature-box-02">
                    <i class="icon theme-bg ti-layout"></i>
                    <div class="feature-content">
                        <h5>Web Design</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <div class="read-more">
                            <a href="#" class="more-btn">Read More <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div> <!-- feature-box-02 -->
            </div> <!-- col -->

            <div class="col-12 col-md-6 col-lg-4">
                <div class="feature-box-02">
                    <i class="icon theme-bg ti-camera"></i>
                        <div class="feature-content">
                        <h5>Responsive Design</h5>
                        <p>All web applications are tested to works in all devices sizes and platforms.</p>
                        <div class="read-more">
                            <a href="#" class="more-btn">Read More <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div> <!-- feature-box-02 -->
            </div> <!-- col -->

            <div class="col-12 col-md-6 col-lg-4">
                <div class="feature-box-02">
                    <i class="icon theme-bg ti-brush-alt"></i>
                    <div class="feature-content">
                        <h5>Web design</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <div class="read-more">
                            <a href="#" class="more-btn">Read More <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div> <!-- feature-box-02 -->
            </div> <!-- col -->

            <div class="col-12 col-md-6 col-lg-4">
                <div class="feature-box-02">
                    <i class="icon theme-bg ti-world"></i>
                    <div class="feature-content">
                        <h5>Mobile apps</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <div class="read-more">
                            <a href="#" class="more-btn">Read More <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div> <!-- feature-box -->
            </div> <!-- col -->
        </div> <!-- row -->
    </div> <!-- container -->
</section>
<!-- Service End -->