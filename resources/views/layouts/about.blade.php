<!-- About us -->
<section id="aboutus" class="section p-0px-b">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="about-left">
                    <h2>I'm José Sabina</h2>
                    <h3>I'm Developer web &amp; SEO consultant based in Spain </h3>
                    <p>I design and develop services for customers of all sizes, specializing in creating stylish, modern websites, web services and online stores.</p>
                    <p>My passion is to develop web pages that provide satisfactory experiences to users through a bold interface and interactions that meet all their needs. Check out my <a class="m-btn-link theme-after" href="#portfolio">Portfolio</a></p>
                    <a class="m-btn m-btn-theme" href="#contact">Contact me <i class="ti-arrow-right"></i></a>
                </div>
            </div> <!-- col -->

            <div class="col-md-6 text-center sm-m-45px-t">
                <img src="/image/template/man.jpg" title="" alt="">
            </div> <!-- col -->

        </div> <!-- row -->
    </div> <!-- container -->
</section>
<!-- / -->