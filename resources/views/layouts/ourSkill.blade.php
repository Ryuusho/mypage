<!-- Our Skill -->
<section class="section skill-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="skill-left p-80px-r md-p-40px-r sm-p-0px-r sm-m-40px-b">
                    <h3 class="font-alt">Make beauty Things With Passion.</h3>
                    <p class="m-30px-b">I design and develop services for customers of all sizes, specializing in creating stylish, modern websites, web services and online stores. My passion is to design digital user experiences through the bold interface.</p>
                    <a href="#" class="m-btn m-btn-theme">Read More <i class="ti-arrow-right"></i></a>
                </div>
            </div>

            <div class="col-md-6">
                <div class="skills sm-m-20px-b">
                    <div class="progress-lt">
                        <h6>HTML5</h6>
                        <span>92%</span>
                        <div class="progress">
                            <div class="progress-bar theme-g-bg" role="progressbar" aria-valuenow="92" aria-valuemin="0" aria-valuemax="100" style="width: 92%;">
                            </div><!-- /progress-bar -->
                        </div><!-- /progress -->
                    </div><!-- /progress-lt -->
                    
                    <div class="progress-lt">
                        <h6>CSS3</h6>
                        <span>84%</span>
                        <div class="progress">
                            <div class="progress-bar theme-g-bg" role="progressbar" aria-valuenow="84" aria-valuemin="0" aria-valuemax="100" style="width: 84%;">
                            </div><!-- /progress-bar -->
                        </div><!-- /progress -->
                    </div><!-- /progress-lt -->

                    <div class="progress-lt">
                        <h6>JavaScript</h6>
                        <span>88%</span>
                        <div class="progress">
                            <div class="progress-bar theme-g-bg" role="progressbar" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100" style="width: 88%;">
                            </div><!-- /progress-bar -->
                        </div><!-- /progress -->
                    </div><!-- /progress-lt -->

                    <div class="progress-lt">
                        <h6>PHP</h6>
                        <span>95%</span>
                        <div class="progress">
                            <div class="progress-bar theme-g-bg" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                            </div><!-- /progress-bar -->
                        </div><!-- /progress -->
                    </div><!-- /progress-lt -->
                    <div class="progress-lt">
                        <h6>LARAVEL</h6>
                        <span>95%</span>
                        <div class="progress">
                            <div class="progress-bar theme-g-bg" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                            </div><!-- /progress-bar -->
                        </div><!-- /progress -->
                    </div><!-- /progress-lt -->
                    <div class="progress-lt">
                        <h6>VUE.JS</h6>
                        <span>85%</span>
                        <div class="progress">
                            <div class="progress-bar theme-g-bg" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                            </div><!-- /progress-bar -->
                        </div><!-- /progress -->
                    </div><!-- /progress-lt -->
                </div>
            </div> <!-- col -->
        </div> <!-- row -->
    </div> <!-- container -->
</section>
<!-- / -->