<!-- Portfolio Start -->
<section id="portfolio" class="section gray-bg">
    <div class="container">
        <div class="row justify-content-center m-60px-b md-m-40px-b sm-m-30px-b">
            <div class="col-12 col-md-10 col-lg-7">
                <div class="section-title text-center">
                    <h2 class="font-alt">Portfolio</h2>
                    <p>I design and develop services for customers of all sizes, specializing in creating stylish, modern websites, web services and online stores. My passion is to design digital user experiences through the bold interface.</p>
                </div>
            </div>
        </div>

        <div class="portfolio-filter m-30px-b">
            <ul class="filter text-center">
                <li class="active" data-filter="*">All</li>
                <li data-filter=".photoshop">Photoshop</li>
                <li data-filter=".website">Website</li>
                <li data-filter=".apps">Apps</li>
            </ul>
        </div> <!-- Portfolio Filter -->

        <div class="portfolio-content" style="position: relative; height: 776.156px;">
            <ul class="portfolio-cols portfolio-cols-3">
                <li class="portfolio-item website" style="position: absolute; left: 0px; top: 0px;">
                    <div class="portfolio-col portfolio-hover-01">
                        <div class="portfolio-img">
                            <a href="#">
                                <img src="/image/template/portfolio-1.jpg" title="" alt="">
                            </a>

                            <div class="hover">
                                <div class="action-btn">
                                    <a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="popup-video theme-color">
                                        <i class="fa fa-play"></i>
                                    </a>

                                    <a class="lightbox-gallery theme-color" href="/image/template/portfolio-1.jpg" title="Lightbox gallery image title...">
                                        <i class="fas fa-expand"></i>
                                    </a>

                                    <a href="#" class="theme-color">
                                        <i class="fa fa-link"></i>
                                    </a>
                                </div> <!-- Video Btn -->
                            </div> <!-- Hover -->
                        </div>

                        <div class="portfolio-info">
                            <h5>Ryan Portpolio Template</h5>
                            <span>Resent Work</span>
                        </div>
                    </div> <!-- Portfolio -->
                </li> <!-- col -->

                <li class="portfolio-item apps" style="position: absolute; left: 379px; top: 0px;">
                    <div class="portfolio-col portfolio-hover-01">
                        <div class="portfolio-img">
                            <a href="#">
                                <img src="/image/template/portfolio-2.jpg" title="" alt="">
                            </a>

                            <div class="hover">
                                <div class="action-btn">
                                    <a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="popup-video theme-color">
                                        <i class="fa fa-play"></i>
                                    </a>

                                    <a class="lightbox-gallery theme-color" href="/image/template/portfolio-2.jpg" title="Lightbox gallery image title...">
                                        <i class="fas fa-expand"></i>
                                    </a>

                                    <a href="#" class="theme-color">
                                        <i class="fa fa-link"></i>
                                    </a>
                                </div> <!-- Video Btn -->
                            </div> <!-- Hover -->
                        </div>

                        <div class="portfolio-info">
                            <h5>Ryan Portpolio Template</h5>
                            <span>Resent Work</span>
                        </div>
                    </div> <!-- Portfolio -->
                </li> <!-- col -->

                <li class="portfolio-item photoshop apps" style="position: absolute; left: 759px; top: 0px;">
                    <div class="portfolio-col portfolio-hover-01">
                        <div class="portfolio-img">
                            <a href="#">
                                <img src="/image/template/portfolio-3.jpg" title="" alt="">
                            </a>
                            <div class="hover">
                                <div class="action-btn">
                                    <a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="popup-video theme-color">
                                        <i class="fa fa-play"></i>
                                    </a>

                                    <a class="lightbox-gallery theme-color" href="/image/template/portfolio-3.jpg" title="Lightbox gallery image title...">
                                        <i class="fas fa-expand"></i>
                                    </a>

                                    <a href="#" class="theme-color">
                                        <i class="fa fa-link"></i>
                                    </a>
                                </div> <!-- Video Btn -->
                            </div> <!-- Hover -->
                        </div>

                        <div class="portfolio-info">
                        <h5>Ryan Portpolio Template</h5>
                        <span>Resent Work</span>
                        </div>
                    </div> <!-- Portfolio -->
                </li> <!-- col -->

                <li class="portfolio-item photoshop website" style="position: absolute; left: 0px; top: 388px;">
                    <div class="portfolio-col portfolio-hover-01">
                        <div class="portfolio-img">
                            <a href="#">
                                <img src="image/template/portfolio-4.jpg" title="" alt="">
                            </a>
                            <div class="hover">
                                <div class="action-btn">
                                    <a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="popup-video theme-color">
                                        <i class="fa fa-play"></i>
                                    </a>
                                    <a class="lightbox-gallery theme-color" href="image/template/portfolio-4.jpg" title="Lightbox gallery image title...">
                                        <i class="fas fa-expand"></i>
                                    </a>
                                    <a href="#" class="theme-color">
                                        <i class="fa fa-link"></i>
                                    </a>
                                </div> <!-- Video Btn -->
                            </div> <!-- Hover -->
                        </div>

                        <div class="portfolio-info">
                        <h5>Ryan Portpolio Template</h5>
                        <span>Resent Work</span>
                        </div>
                    </div> <!-- Portfolio -->
                </li> <!-- col -->

                <li class="portfolio-item photoshop apps" style="position: absolute; left: 379px; top: 388px;">
                    <div class="portfolio-col portfolio-hover-01">
                        <div class="portfolio-img">
                            <a href="#">
                                <img src="/image/template/portfolio-5.jpg" title="" alt="">
                            </a>
                            <div class="hover">
                                <div class="action-btn">
                                <a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="popup-video theme-color">
                                    <i class="fa fa-play"></i>
                                </a>
                                <a class="lightbox-gallery theme-color" href="/image/template/portfolio-5.jpg" title="Lightbox gallery image title...">
                                    <i class="fas fa-expand"></i>
                                </a>
                                <a href="#" class="theme-color">
                                    <i class="fa fa-link"></i>
                                </a>
                                </div> <!-- Video Btn -->
                            </div> <!-- Hover -->
                        </div>

                        <div class="portfolio-info">
                        <h5>Ryan Portpolio Template</h5>
                        <span>Resent Work</span>
                        </div>
                    </div> <!-- Portfolio -->
                </li> <!-- col -->

                <li class="portfolio-item app website" style="position: absolute; left: 759px; top: 388px;">
                    <div class="portfolio-col portfolio-hover-01">
                        <div class="portfolio-img">
                            <a href="#">
                                <img src="/image/template/portfolio-6.jpg" title="" alt="">
                            </a>
                            <div class="hover">
                                <div class="action-btn">
                                    <a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="popup-video theme-color">
                                        <i class="fa fa-play"></i>
                                    </a>
                                    <a class="lightbox-gallery theme-color" href="/image/template/portfolio-6.jpg" title="Lightbox gallery image title...">
                                        <i class="fas fa-expand"></i>
                                    </a>
                                    <a href="#" class="theme-color">
                                        <i class="fa fa-link"></i>
                                    </a>
                                </div> <!-- Video Btn -->
                            </div> <!-- Hover -->
                        </div>

                        <div class="portfolio-info">
                        <h5>Ryan Portpolio Template</h5>
                        <span>Resent Work</span>
                        </div>
                    </div> <!-- Portfolio -->
                </li> <!-- col -->
            </ul> <!-- row -->
        </div> <!-- portfolio content -->
    </div> <!-- Container -->
</section>
<!--  Portfolio End  -->