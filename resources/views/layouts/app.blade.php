<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.header.links')
</head>
<body data-spy="scroll" data-target="#navbarRyan" data-offset="98">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WS43MN9"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
    <div id="app">
        <div id="loading">
            <div class="load-circle-black"><span class="one"></span></div>
        </div>
        <main >
            @yield('content')
        </main>
        
        @include('layouts.footer')
        @stack('scripts')
    </div>
</body>
</html>
