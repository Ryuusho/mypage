<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139026337-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-139026337-1');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WS43MN9');</script>
<!-- End Google Tag Manager -->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="wUG-TCUavj91AjrC0hhw6dpKPNS4hImSl5s9_AyHOW4" />
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title> @yield('title')</title>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">


 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.6.3/css/all.min.css">
<link href="{{ asset('/css/template/themify-icons.css') }}" rel="stylesheet">
<!-- / -->

<!-- Plugin CSS -->
<link href="{{ asset('/css/template/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('/css/template/owl.carousel.min.css') }}" rel="stylesheet">
<link href="{{ asset('/css/template/magnific-popup.css') }}" rel="stylesheet">
<!-- / -->

<!-- Theme Style -->
<link href="{{ asset('/css/template/styles.css') }}" rel="stylesheet">
<link href="{{ asset('/css/template/default.css') }}" rel="stylesheet" id="color_theme">
<!-- / -->

<!-- Favicon -->
<link rel="icon" href="/image/template/favicon.ico">



{{-- ============================= Scripts ============================= --}}

<!-- jQuery -->
<script src="{{ asset('/js/template/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('/js/template/jquery-migrate-3.0.0.min.js') }}"></script>

<!-- Plugins -->
<script src="{{ asset('/js/template/popper.min.js')}}"></script>
<script src="{{ asset('/js/template/bootstrap.min.js')}}"></script>
<script src="{{ asset('/js/template/owl.carousel.min.js') }}"></script>
<script src="{{ asset('/js/template/isotope.pkgd.min.js')}}"></script>
<script src="{{ asset('/js/template/jquery.magnific-popup.min.js')}}"></script>
<!-- custom -->
<script src="{{ asset('/js/template/custom.js') }}"></script>
