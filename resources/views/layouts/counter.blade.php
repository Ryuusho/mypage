    <!-- Counter Start -->
<section class="counter-section">
    <div class="container">
      <div class="counter-row">
        <div class="row">
            <div class="col-6 col-md-3 col-sm-6 wow fadeInLeft">
                <div class="counter-col">
                    <div class="counter-data" data-count="375">
                        <div class="count">375</div>
                        <h6>Happy Clients</h6>
                    </div>
                </div>
            </div>
            <!-- col -->

            <div class="col-6 col-md-3 col-sm-6 wow fadeInLeft">
                <div class="counter-col">
                    <div class="counter-data" data-count="375">
                        <div class="count">375</div>
                        <h6>Telephonic Talk</h6>
                    </div>
                </div>
            </div>
            <!-- col -->

            <div class="col-6 col-md-3 col-sm-6 wow fadeInLeft">
                <div class="counter-col">
                    <div class="counter-data" data-count="550">
                        <div class="count">550</div>
                        <h6>Photo Capture</h6>
                    </div>
                </div>
            </div>
            <!-- col -->

            <div class="col-6 col-md-3 col-sm-6 wow fadeInLeft">
                <div class="counter-col">
                    <div class="counter-data" data-count="450">
                        <div class="count">450</div>
                        <h6>Project</h6>
                    </div>
                </div>
            </div>
            <!-- col -->
        </div>
        <!-- row -->
      </div>
    </div>
    <!-- container -->
</section>
<!-- Counter End -->