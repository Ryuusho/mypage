<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Mail\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
	public function index()
	{
		
	}

	public function store(Request $request)
	{ 
		$contact = Contact::create(request()->all());
		Mail::to('JSBalcaza@gmail.com')->send(new ContactUs($request->all()));

		return ['status' => 'success', 'data' => $contact];

	}

	public function destroy($id)
	{
		$contact = Contact::find($id);
		$contact->delete();

		return ['message' => 'Se elimino el mensaje correctamente!', 'data' => $contact];
	}

}
