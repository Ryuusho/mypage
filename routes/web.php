<?php

use Spatie\Sitemap\SitemapGenerator;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sitemap',function(){
	SitemapGenerator::create(request()->getSchemeAndHttpHost())->writeToFile('sitemap.xml');
	return response()->file($_SERVER['DOCUMENT_ROOT'].'/sitemap.xml');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/download', function () {
	return response()->download(public_path().'/curriculum/curriculum.docx');
});
